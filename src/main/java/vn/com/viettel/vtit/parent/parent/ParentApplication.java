package vn.com.viettel.vtit.parent.parent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParentVtitApplication.class, args);
	}

}
